import json
import io
with open("latestSchemaMod.json",'r') as file:            
    schema_json = file.read()
    output_directory = "kafkaSchema"
    from avrogen import write_schema_files
    write_schema_files(schema_json, output_directory)