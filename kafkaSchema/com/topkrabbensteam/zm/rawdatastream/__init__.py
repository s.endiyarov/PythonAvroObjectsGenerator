from .....schema_classes import SchemaClasses
Advertisement = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.AdvertisementClass
Building = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.BuildingClass
Floor = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.FloorClass
Geo = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.GeoClass
MediaFiles = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.MediaFilesClass
Object = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.ObjectClass
ObjectData = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.ObjectDataClass
ObjectHeader = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.ObjectHeaderClass
Owner = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.OwnerClass
PhotoObject = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PhotoObjectClass
Premises = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PremisesClass
Prices = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PricesClass
PropertyHeader = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PropertyHeaderClass
