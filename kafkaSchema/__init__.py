

from .schema_classes import SchemaClasses, SCHEMA as my_schema, get_schema_type
from avro.io import DatumReader


class SpecificDatumReader(DatumReader):
    SCHEMA_TYPES = {
        "com.topkrabbensteam.zm.rawdatastream.Advertisement": SchemaClasses.com.topkrabbensteam.zm.rawdatastream.AdvertisementClass,
        "com.topkrabbensteam.zm.rawdatastream.Building": SchemaClasses.com.topkrabbensteam.zm.rawdatastream.BuildingClass,
        "com.topkrabbensteam.zm.rawdatastream.Floor": SchemaClasses.com.topkrabbensteam.zm.rawdatastream.FloorClass,
        "com.topkrabbensteam.zm.rawdatastream.Geo": SchemaClasses.com.topkrabbensteam.zm.rawdatastream.GeoClass,
        "com.topkrabbensteam.zm.rawdatastream.MediaFiles": SchemaClasses.com.topkrabbensteam.zm.rawdatastream.MediaFilesClass,
        "com.topkrabbensteam.zm.rawdatastream.Object": SchemaClasses.com.topkrabbensteam.zm.rawdatastream.ObjectClass,
        "com.topkrabbensteam.zm.rawdatastream.ObjectData": SchemaClasses.com.topkrabbensteam.zm.rawdatastream.ObjectDataClass,
        "com.topkrabbensteam.zm.rawdatastream.ObjectHeader": SchemaClasses.com.topkrabbensteam.zm.rawdatastream.ObjectHeaderClass,
        "com.topkrabbensteam.zm.rawdatastream.Owner": SchemaClasses.com.topkrabbensteam.zm.rawdatastream.OwnerClass,
        "com.topkrabbensteam.zm.rawdatastream.PhotoObject": SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PhotoObjectClass,
        "com.topkrabbensteam.zm.rawdatastream.Premises": SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PremisesClass,
        "com.topkrabbensteam.zm.rawdatastream.Prices": SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PricesClass,
        "com.topkrabbensteam.zm.rawdatastream.PropertyHeader": SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PropertyHeaderClass,
    }
    def __init__(self, readers_schema=None, **kwargs):
        writers_schema = kwargs.pop("writers_schema", readers_schema)
        writers_schema = kwargs.pop("writer_schema", writers_schema)
        super(SpecificDatumReader, self).__init__(writers_schema, readers_schema, **kwargs)
    def read_record(self, writers_schema, readers_schema, decoder):
        
        result = super(SpecificDatumReader, self).read_record(writers_schema, readers_schema, decoder)
        
        if readers_schema.fullname in SpecificDatumReader.SCHEMA_TYPES:
            result = SpecificDatumReader.SCHEMA_TYPES[readers_schema.fullname](result)
        
        return result