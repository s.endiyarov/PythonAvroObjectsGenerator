import json
from kafkaSchema import my_schema
from avro import datafile, io
from kafkaSchema.com.topkrabbensteam.zm.rawdatastream import Advertisement,Geo,MediaFiles,Object,ObjectData,Owner,Prices, \
Building,Floor,ObjectHeader,PhotoObject,Premises,PropertyHeader

advert = Advertisement()
advert.uuid = "123123-123123-234234"
advert.type = 1
advert.source = "cian"
advert.url = "http://cian.ru"

mediaFile = MediaFiles()
mediaFile.type = 1
mediaFile.source = "http:\\hello.jpg"
advert.media_files.append(mediaFile)

prices = Prices()
prices.type = 1
prices.type_comment = "type 1"
prices.value = 123445.0
prices.value_type = 3
advert.prices.append(prices)

advert.owner.name = "Ivan"
advert.owner.email = "Ivan@e1.ru"
advert.owner.phone = "32167111"

advert.object.type = 1
advert.object.possible_use ="no use at all"
advert.object.business_offered = False

advert.object.object_data.type = 5
advert.object.object_data.building.property_header.obj_header.obj_uri ="some uri"
advert.object.object_data.premises.area_type = 3
advert.object.object_data.premises.cadastral_value = 334567.3
advert.object.object_data.premises.floor.is_last = 0
advert.object.object_data.premises.floor.level = 5
advert.object.object_data.premises.photo.inspector_id="mm@mail.ru"
advert.object.object_data.premises.photo.photo_name="photoTitle"


stringObject = advert.__str__()
stringObject = stringObject.replace("None","null")
stringObject= stringObject.replace("False","false")
stringObject = stringObject.replace("True","true")
stringObject = stringObject.replace("False","false")
#stringObject = stringObject.replace("'","\"")

with open('writtenAsJson.json','w') as jsonFile:
    jsonFile.write(stringObject)

with open('writtenObject.binary', 'w+b') as f:
    writer = datafile.DataFileWriter(f,io.DatumWriter(), my_schema)
    writer.append(advert)
    writer.close()

