import json
from test import my_schema
from avro import datafile, io
from test.com.example import Customer, CustomerAddress


myCustomer = Customer()
myCustomer.age = 33
myCustomer.first_name = "FirstName"
myCustomer.middle_name = "Valera"
myCustomer.last_name = "Dyadya"
 

myCustomer.customer_address.address="Novatorv street, Russia, Yekaterinburg"
myCustomer.customer_address.postcode="620098"




with open('writtenAsJson.json','w') as jsonFile:
    jsonFile.write(myCustomer.__str__())

with open('writtenObject.binary', 'w+b') as f:
    writer = datafile.DataFileWriter(f,io.DatumWriter(), my_schema)
    writer.append(myCustomer)
    writer.close()

